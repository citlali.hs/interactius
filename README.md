# FD Experiencias Interactivas

Repositorio de la clase de Fundamentos del Diseño de Experiencias Interactivas de Elisava, curso 2023-2024.

## Referentes

Dispuestos en un gráfico de dos ejes: horizontal (narrativa — función) y vertical (indicar — hacer luz).

```
                    hacer luz
            (1)         |         (4)
                        |         (5)
                        |
            (8)         |
  narrativa –––––––––––(9)––––––––––– función
            (7)         |
                        |
                        |         (3)
            (6)         |         (2)
                     indicar
```

- (1) Interruptora: lámpara que se apaga cuando recibes una notificación en el móvil
- (2) Notificadora: lámpara que se enciende cuando recibes una notificación en el móvil
- (3) [Simon](https://es.wikipedia.org/wiki/Simon_(juego)), juego electrónico
- (4) [Instalación interactiva](https://www.flickr.com/photos/danielarmengolaltayo/albums/72157696451678665/) que modifica la interfaz del Simon  
- (5) Sistema luces WC Elisava
- (6) [Remote Pulse](https://www.lozano-hemmer.com/remote_pulse.php)
- (7) [Light Kinetics](https://www.espadaysantacruz.com/projects/light-kinetics)
- (8) [Constellaction](https://pangenerator.com/projects/constellaction/) (estado 3, efecto dominó con la luz)
- (9) [Friendship Lamps](https://www.friendlamps.com/)

## Enlaces

- [Instrucciones montaje lámpara](https://gitlab.com/citlali.hs/interactius/-/raw/main/instrucciones.pdf)
    - Cortar la caja de la lámpara con cuidado por donde se dobla, se usarán varias partes durante el montaje
    - Antes de pegar la tira LED, enrollarla para ver como quedará, va justa y una vez pegada resulta engorroso hacer correcciones
- Esquema conexiones electrónica [(1)](https://gitlab.com/citlali.hs/interactius/-/raw/main/esquema.pdf) [(2)](https://gitlab.com/citlali.hs/interactius/-/raw/main/esquema2.pdf)
    - La conexión del sensor de luz (LDR) se debe extender para que quede fuera de la lámpara
- [Código Arduino](lampara/lampara.ino)
- [Repositorio de Sensores y Actuadores](https://github.com/linalab/sensors_library)
- [Lámpara virtual](https://wokwi.com/projects/399231712062414849)

## Ejemplos de comportamiento

Para probar los ejemplos hay que copiarlos dentro del estado 4.

```java
// la lámpara parpadea a un ritmo constante
digitalWrite(pinRele, HIGH);
digitalWrite(pinLed1, LOW);
digitalWrite(pinLed2, LOW);
digitalWrite(pinLed3, LOW);
digitalWrite(pinLed4, HIGH);
delay(500);
digitalWrite(pinRele, LOW);
delay(500);
```

```java
// la lámpara parpadea según la cantidad de luz que detecta el sensor
digitalWrite(pinRele, HIGH);
digitalWrite(pinLed1, LOW);
digitalWrite(pinLed2, LOW);
digitalWrite(pinLed3, LOW);
digitalWrite(pinLed4, HIGH);
delay(analogRead(pinSensorAnalogico) + 70);
digitalWrite(pinRele, LOW);
delay(analogRead(pinSensorAnalogico) + 70);
```
