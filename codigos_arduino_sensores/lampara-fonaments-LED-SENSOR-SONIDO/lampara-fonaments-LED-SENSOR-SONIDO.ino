// 4 estados (sin contar el de espera)
// cada estado se activa manteniendo apretado un botón 
// 4 leds, cada uno vinculado a un estado distinto a modo de interfaz
// en el loop se leen los botones en orden, es decir, se prioriza la selección del estado menor

int pinRele = 3;
int pinLed1 = 4;
int pinLed2 = 5;
int pinLed3 = 6;
int pinLed4 = 7;
int pinBoton1 = 8;
int pinBoton2 = 9;
int pinBoton3 = 10;
int pinBoton4 = 11;
int pinSensorAnalogico = A0;
int estado = 0;
boolean pulsoEnviado = false;
// RGB
int pinR = 13;
int pinG = 12;
int pinB = 2;

void setup() {
  Serial.begin(9600);
    pinMode(pinRele, OUTPUT);
    pinMode(pinLed1, OUTPUT);
    pinMode(pinLed2, OUTPUT);
    pinMode(pinLed3, OUTPUT);
    pinMode(pinLed4, OUTPUT);
    pinMode(pinBoton1, INPUT_PULLUP);
    pinMode(pinBoton2, INPUT_PULLUP);
    pinMode(pinBoton3, INPUT_PULLUP);
    pinMode(pinBoton4, INPUT_PULLUP);
    pinMode(pinR,OUTPUT);
    pinMode(pinG,OUTPUT);
    pinMode(pinB,OUTPUT);

}

void loop() {
  
    if(digitalRead(pinBoton1) == LOW){
        estado = 1;
    }else if(digitalRead(pinBoton2) == LOW){
        estado = 2;
    }else if(digitalRead(pinBoton3) == LOW){
        estado = 3;
    }else if(digitalRead(pinBoton4) == LOW){
        estado = 4;
    }else{
        estado = 0;
    }
    switch (estado){
        case 0:
            // lámpara apagada (estado de espera)
            digitalWrite(pinRele, LOW);
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            break;
        case 1:
            // lámpara encendida
            digitalWrite(pinRele, HIGH);
            digitalWrite(pinLed1, HIGH);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            break;
        case 2:
            // lámpara responde al sensor
            // (se puede usar de forma independiente o con otras lámparas)
            Serial.println(analogRead(pinSensorAnalogico));
            if(analogRead(pinSensorAnalogico) < 500){
                digitalWrite(pinRele, HIGH);
            }else{
                digitalWrite(pinRele, LOW);
            }
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, HIGH);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            delay(100);
            break;
        case 3:
            // estado para la interacción con otras lámparas
            // envía un pulso para generar un efecto de ola
            delay(50); // delay lectura sensor
            if(analogRead(pinSensorAnalogico) < 300){
                if(pulsoEnviado == false){
                    delay(200); // delay reacción a la ola
                    digitalWrite(pinRele, HIGH);
                    delay(200); // delay duración pulso
                    digitalWrite(pinRele, LOW); 
                    pulsoEnviado = true;
                }
            }else{
                pulsoEnviado = false;
            }
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, HIGH);
            digitalWrite(pinLed4, LOW);
            break;
        case 4:
            // estado para el código del proyecto
            // (se pueden crear más estados si es necesario)
            // Enciendo LED indicador ROJO 4
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, HIGH);
            //
            digitalWrite(pinR,HIGH);
            digitalWrite(pinG,LOW);
            digitalWrite(pinB,HIGH);
            delay(1000);
            digitalWrite(pinR,LOW);
            digitalWrite(pinG,HIGH);
            digitalWrite(pinB,HIGH);
            delay(1000);
            digitalWrite(pinR,HIGH);
            digitalWrite(pinG,HIGH);
            digitalWrite(pinB,HIGH);
            break;
    }
}
