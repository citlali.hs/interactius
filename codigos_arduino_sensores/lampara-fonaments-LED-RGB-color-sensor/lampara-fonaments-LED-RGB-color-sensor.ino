// 4 estados (sin contar el de espera)
// cada estado se activa manteniendo apretado un botón 
// 4 leds, cada uno vinculado a un estado distinto a modo de interfaz
// en el loop se leen los botones en orden, es decir, se prioriza la selección del estado menor

int pinRele = 3;
int pinLed1 = 4;
int pinLed2 = 5;
int pinLed3 = 6;
int pinLed4 = 7;
int pinBoton1 = 8;
int pinBoton2 = 9;
int pinBoton3 = 10;
int pinBoton4 = 11;
int pinSensorAnalogico = A0;
int estado = 0;
boolean pulsoEnviado = false;
// RGB
int pinR = 13;
int pinG = 2;
int pinB = 12;


#define s0 A1        //Module pins  wiring
#define s1 A2
#define s2 A3
#define s3 A4
#define out A5

int  Red=0, Blue=0, Green=0;  //RGB values 


void setup() {

   pinMode(s0,OUTPUT);    //pin modes
   pinMode(s1,OUTPUT);
   pinMode(s2,OUTPUT);
   pinMode(s3,OUTPUT);
   pinMode(out,INPUT);   
   analogWrite(s0,255); //Putting S0/S1 on HIGH/HIGH levels  means the output frequency scalling is at 100% (recommended)
   analogWrite(s1,255);  //LOW/LOW is off HIGH/LOW is 20% and LOW/HIGH is  2%
  
  
  Serial.begin(9600);
    pinMode(pinRele, OUTPUT);
    pinMode(pinLed1, OUTPUT);
    pinMode(pinLed2, OUTPUT);
    pinMode(pinLed3, OUTPUT);
    pinMode(pinLed4, OUTPUT);
    pinMode(pinBoton1, INPUT_PULLUP);
    pinMode(pinBoton2, INPUT_PULLUP);
    pinMode(pinBoton3, INPUT_PULLUP);
    pinMode(pinBoton4, INPUT_PULLUP);
    pinMode(pinR,OUTPUT);
    pinMode(pinG,OUTPUT);
    pinMode(pinB,OUTPUT);

}


void GetColors()  
{    
  analogWrite(s2,  0);                                           //S2/S3 levels define which set  of photodiodes we are using LOW/LOW is for RED LOW/HIGH is for Blue and HIGH/HIGH  is for green 
  analogWrite(s3, 0);                                           
  Red = pulseIn(out, analogRead(out) == 255 ? 0 : 255);       //here we wait  until "out" go LOW, we start measuring the duration and stops when "out" is  HIGH again, if you have trouble with this expression check the bottom of the code
  delay(20);  
  analogWrite(s3, 255);                                         //Here  we select the other color (set of photodiodes) and measure the other colors value  using the same techinque
  Blue = pulseIn(out, analogRead(out) == 255 ? 0  : 255);
  delay(20);  
  analogWrite(s2, 255);  
  Green = pulseIn(out,  analogRead(out) == 255 ? 0 : 255);
  delay(20);  
}

void loop() {
  
    if(digitalRead(pinBoton1) == LOW){
        estado = 1;
    }else if(digitalRead(pinBoton2) == LOW){
        estado = 2;
    }else if(digitalRead(pinBoton3) == LOW){
        estado = 3;
    }else if(digitalRead(pinBoton4) == LOW){
        estado = 4;
    }else{
        estado = 0;
    }
    switch (estado){
        case 0:
            // lámpara apagada (estado de espera)
            digitalWrite(pinRele, LOW);
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            break;
        case 1:
            // lámpara encendida
            digitalWrite(pinRele, HIGH);
            digitalWrite(pinLed1, HIGH);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            break;
        case 2:
            // lámpara responde al sensor
            // (se puede usar de forma independiente o con otras lámparas)
            Serial.println(analogRead(pinSensorAnalogico));
            if(analogRead(pinSensorAnalogico) < 500){
                digitalWrite(pinRele, HIGH);
            }else{
                digitalWrite(pinRele, LOW);
            }
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, HIGH);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, LOW);
            delay(100);
            break;
        case 3:
            // estado para la interacción con otras lámparas
            // envía un pulso para generar un efecto de ola
            delay(50); // delay lectura sensor
            if(analogRead(pinSensorAnalogico) < 300){
                if(pulsoEnviado == false){
                    delay(200); // delay reacción a la ola
                    digitalWrite(pinRele, HIGH);
                    delay(200); // delay duración pulso
                    digitalWrite(pinRele, LOW); 
                    pulsoEnviado = true;
                }
            }else{
                pulsoEnviado = false;
            }
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, HIGH);
            digitalWrite(pinLed4, LOW);
            break;
        case 4:
        // estado para el código del proyecto
            // (se pueden crear más estados si es necesario)
            // Enciendo LED indicador ROJO 4
            digitalWrite(pinLed1, LOW);
            digitalWrite(pinLed2, LOW);
            digitalWrite(pinLed3, LOW);
            digitalWrite(pinLed4, HIGH);
            //

        GetColors();                                     //Execute the GetColors function  to get the value of each RGB color
                                                   //Depending  of the RGB values given by the sensor we can define the color and displays it on  the monitor

  if (Red <=15 && Green <=15 && Blue <=15)         //If the values  are low it's likely the white color (all the colors are present)
      {
        Serial.println("White");                    
      }
  else if (Red<Blue && Red<=Green && Red<23) {
  //if  Red value is the lowest one and smaller thant 23 it's likely Red
            digitalWrite(pinR,LOW);
            digitalWrite(pinG,HIGH);
            digitalWrite(pinB,HIGH);
            delay(1000);
      Serial.println("Red");
  }
  else if (Blue<Green && Blue<Red && Blue<20)    //Same thing for Blue
  {
      Serial.println("Blue");
      digitalWrite(pinR,HIGH);
            digitalWrite(pinG,HIGH);
            digitalWrite(pinB,LOW);
            delay(1000);
  }
  else if (Green < Red && Green-Blue<= 8)           //Green it was a little tricky,  you can do it using the same method as above (the lowest), but here I used a reflective  object
      {
      Serial.println("Green");  
      digitalWrite(pinR,HIGH);
            digitalWrite(pinG,LOW);
            digitalWrite(pinB,HIGH);
            delay(1000);
      }
      //which means the  blue value is very low too, so I decided to check the difference between green and  blue and see if it's acceptable

  else
     {
      Serial.println("Unknown");                  //if the color is not recognized, you can add as many as you want
     }


  delay(100);                                   //2s delay you can modify if you  want
        
            
            digitalWrite(pinR,HIGH);
            digitalWrite(pinG,HIGH);
            digitalWrite(pinB,HIGH);
            break;
    }
}
