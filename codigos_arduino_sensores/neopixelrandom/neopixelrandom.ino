#include <Adafruit_NeoPixel.h>
#define BOTON1 13
#define BOTON2 A1
#define PIN 11
#define NUMPIXELS 60
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  Serial.begin(9600);
  pinMode(12,INPUT_PULLUP);
 pixels.begin();

}

void loop() {
  int valorBOTON1 = digitalRead(BOTON1);
  int valorBOTON2 = analogRead(BOTON2);
  Serial.println(valorBOTON2);

  if(valorBOTON2 < 100){
    pixels.clear();
    for(int i = 0; i < NUMPIXELS; i++){
      pixels.setPixelColor(i, pixels.Color(0,255,0));
    }
    pixels.show();
    delay(200);
}

  if(valorBOTON1 == 0){
    //Serial.println(valorBOTON1);
    pixels.clear();
    for(int i = 0; i < NUMPIXELS; i++){
      pixels.setPixelColor(i, pixels.Color(255,0,0));
    }
    pixels.show();
    delay(200);
}

/*if (valorBOTON1 == 1){
  pixels.clear();
  for(int i = 0; i < NUMPIXELS; i++){
    pixels.setPixelColor(i, pixels.Color(0,0,255));
    }
    pixels.show();
    delay(200);
  }
*/

}
